FROM debian:buster-slim AS builder
ARG KBX_APP_VERSION=7.92
ARG VER=$KBX_APP_VERSION

RUN apt update \
 && apt install -y \
    build-essential \
    bzip2 \
    libssh2-1-dev \
    libssl-dev \
    python2.7 \
    wget
RUN wget -nv https://nmap.org/dist/nmap-$VER.tar.bz2
RUN bzip2 -cd nmap-$VER.tar.bz2 | tar xf -
RUN cd nmap-$VER \
 && ./configure \
 && make \
 && make install
RUN rm -fr /nmap-*
RUN apt install -y \
    gtk2-engines-pixbuf \
    python-gtk2

RUN mkdir /kaboxer \
 && echo $KBX_APP_VERSION > /kaboxer/version
